    public hangman        
.model small
.stack 100h
.data
HANG db 20 dup(36d)     
LEN dw 1 dup (0) 
INP db 20 dup (32d)
dash db 20 dup (95d)
enter DB 'ENTER A WORD$'  
winner DB 'YOU SAVED THE MAN!! ^_^$' 
loser DB 'YOU JUST HANGED THE MAN! -_-$' 
c1 db 1 dup(0)   
c2 db 1 dup(0) 
.code

print proc
    push ax
    PUSH BX
    push cx
    push dx 
    MOV AH,02
    MOV BH,0 
    MOV DH,14 ; ROW
    MOV DL,8 ; COL
    int 10h
    mov bl,1111b  
    mov si,0
    MOV BH,0 ; PAGE
   
    p:
    cmp INP[si],'$'
    je back 
    mov ah,09
	mov bh,0
	mov al,INP[si]
	
	mov cx,1
	int 10h
	inc si
	inc dl
	inc dl
	mov ah,02
	int 10h
	
	jmp p
	back:
	
    pop dx
    pop cx
    POP BX
    pop ax
    ret
print endp


compare proc 
     push dx
     
     push bx
     push ax
     
    ;lea di,hang      
     mov di,0
     x:
    
         mov bl,36d
         cmp hang[di],bl
         je r2
         cmp hang[di],al
         je z
         inc di
         jmp x
     z:  
         inc cx 
         inc c1
         mov INP[di],al   
         inc di
         jmp x 
     r2:
         cmp c1,0
         jne printt
         inc c2
     printt:
         call print
         ;pop ax
         mov ax,1000
     
     sd:
        mov bx,100
     sf:
         dec bx
         cmp bx,0
         jne sf
         dec ax
         cmp ax,0
         jne sd 
         pop ax
         pop bx      
         pop dx
         ret
compare endp


hangman proc
        push di
        push si
        push ax
        push bx
        push cx
        push dx
        xor ax,ax
        xor bx,bx
        xor cx,cx
        xor dx,dx 
        
        mov ax,@data
        mov ds,ax   
        
        mov AH, 0   
        mov AX, 12h
        int 10h
           
        MOV  AH, 11  ;function  OBh 
    MOV  BH, 0  ;select  background  color 
    MOV  BL,9 ;background color 7,11,15 possibly
    INT	10H
        MOV AH,02
        MOV BH,0 
        MOV DH,11 ; ROW
        MOV DL,8 ; COL
        int 10h
        mov bl,1111b  
        mov si,0
        MOV BH,0 ; PAGE
       
        pq:
            cmp enter[si],'$'
            je mn 
            mov ah,09
        	mov bh,0
        	mov al,enter[si]
        	
        	mov cx,1
        	int 10h
        	inc si
        	inc dl
        	mov ah,02
        	int 10h
        	
        	jmp pq
                
       mn:
       
            xor ax,ax
            xor bx,bx
            xor cx,cx
            xor dx,dx
            
            mov si,0
            xor cx,cx
            xor bx,bx
        
      NAME_IN: 
            MOV AH, 0 
            INT 16h
            CMP AH, 1CH
            JE FINISH_N   
            MOV HANG[SI], AL
            INC SI    
            inc len
            JMP NAME_IN
        
      FINISH_N:  
            inc LEN
            MOV AH,2 
            MOV DH,15 ; ROW
            MOV DL,8 ; COL
            ;MOV BH,0 ; PAGE
            INT 10H 
            mov bl,24h 
            mov HANG[si],bl
            mov dash[si],bl
            mov inp[si],bl  
           
            mov bl,1111b  
            mov si,0

        dash1:
        
            cmp dash[si],'$'
            je fashi 
            mov ah,09
        	mov bh,0
        	mov al,dash[si]
        	
        	mov cx,1
        	int 10h
        	inc si
        	inc dl
    	    inc dl
        	mov ah,02
        	int 10h
        	
        	jmp dash1 
    	
        fashi:
            mov dash[si],95d
            push ax
            push bx
            push dx
            push cx
            mov cx, 350
            mov dx, 340
                 
            mov al,1111b
            
        dandi1: 
              cmp dx,100
              jl dandi2 
              MOV  AH,0CH
              INT  10H
              dec dx
              jmp dandi1
              
        dandi2:
              cmp cx,425
              jg dori
              MOV  AH,0CH
              INT  10H
              inc cx
              jmp dandi2
            
        dori:
              cmp dx,120
              jg dandi3
              MOV  AH,0CH
              INT  10H
              inc dx
              jmp dori
        dandi3:
              mov cx,320
              mov dx,340
        d3:
              cmp cx,380
              jg donef
              MOV  AH,0CH
              INT  10H
              inc cx
              jmp d3
              
        donef:
              pop cx
              pop dx
              pop bx
              pop ax
             
            
        NEXT_: 
            MOV AH, 0 
            INT 16h
            lea di,HANG 
            mov c1,0  
            ;CMP CX, LEN
            call compare  
            CMP CX, LEN
            JE win
            CMP c2,9
            JE h9 
            cmp c2,1
            je h1
            cmp c2,2
            je h2
            cmp c2,3
            je h3
            cmp c2,4
            je h4
            cmp c2,5
            je h5
            cmp c2,6
            je h6
            cmp c2,7
            je h7
            cmp c2,8
            je h8
            
            jmp NEXT_
              h1:  
              push cx
              push dx
              push ax
              
              mov cx, 395
              mov dx, 120
                 
              mov al,1111b
                lp1:
                    MOV  AH,0CH
                    INT  10H
                    cmp cx,455d
                    jg  lp2
                    inc cx
                    jmp lp1
                lp2: 
                    cmp dx,170
                    jg  lp3
                    
                    MOV  AH,0CH
                    INT  10H
                    inc dx
                    jmp lp2  
                lp3: 
                    cmp cx,395
                    jl  lp4
                    
                    MOV  AH,0CH
                    INT  10H
                    dec cx
                    jmp lp3  
                lp4: 
                    cmp dx,120
                    jl  ex
                    
                    MOV  AH,0CH
                    INT  10H
                    dec dx
                    jmp lp4    
                    
                  ex:
                   pop ax
                  pop dx
                  pop cx 
                  JMP NEXT_
                  
           h2: 
              push cx
              push dx
              push ax
              mov al,1111b
              mov cx,425
              mov dx,170
              danda:
              cmp dx,270
              jg ex2
              MOV  AH,0CH
              INT  10H 
              inc dx
              jmp danda
              
             ex2: 
                  pop ax
                  pop dx
                  pop cx
                  JMP NEXT_
          h3:
              push cx
              push dx
              push ax
              mov al,1111b
              mov dx,200
              mov cx,380 
              haat1:
              cmp cx,425
              jg ex3
              MOV  AH,0CH
              INT  10H 
              inc cx
              jmp haat1
              
             ex3:
              pop ax
              pop dx
              pop cx
              JMP NEXT_
          h4:
           push cx
              push dx
              push ax
              mov al,1111b
              mov dx,200
              mov cx,425 
              haat2:
              cmp cx,470
              jg ex4
              MOV  AH,0CH
              INT  10H 
              inc cx 
              jmp haat2
              
             ex4:
              pop ax
              pop dx
              pop cx
              JMP NEXT_ 
              
              
              h5:
               push cx
              push dx
              push ax
              mov al,1111b
              mov dx,270
              mov cx,425 
              pa1:
              cmp cx,380
              jl ex5
              
              MOV  AH,0CH
              INT  10H 
              dec cx
              inc dx
              jmp pa1
              
             ex5:
              pop ax
              pop dx
              pop cx
              JMP NEXT_
              
              
              h6:
              push cx
              push dx
              push ax
              mov al,1111b
              mov dx,270
              mov cx,425 
              pa2:
              cmp cx,470
              jg ex6
              
              MOV  AH,0CH
              INT  10H 
              inc cx
              inc dx
              jmp pa2
              
             ex6:
              pop ax
              pop dx
              pop cx
              JMP NEXT_ 
              
              h7:
              push cx
              push dx
              push ax
              
                mov cx, 410
                mov dx, 130
                 
                 mov al,1111b
                lp71:
                    MOV  AH,0CH
                    INT  10H
                    cmp cx,420d
                    jg  lp72
                    inc cx
                    jmp lp71
                lp72: 
                    cmp dx,140
                    jg  lp73
                    
                    MOV  AH,0CH
                    INT  10H
                    inc dx
                    jmp lp72  
                lp73: 
                    cmp cx,410
                    jl  lp74
                    
                    MOV  AH,0CH
                    INT  10H
                    dec cx
                    jmp lp73  
                lp74: 
                    cmp dx,130
                    jl  ex7
                    
                    MOV  AH,0CH
                    INT  10H
                    dec dx
                    jmp lp74    
                    
                  ex7:
                   pop ax
                  pop dx
                  pop cx 
                  JMP NEXT_
              h8:
              push cx
              push dx
              push ax
              
                mov cx, 430
                mov dx, 130
                 
                 mov al,1111b
                lp81:
                    MOV  AH,0CH
                    INT  10H
                    cmp cx,440d
                    jg  lp82
                    inc cx
                    jmp lp81
               lp82: 
                    cmp dx,140
                    jg  lp83
                    
                    MOV  AH,0CH
                    INT  10H
                    inc dx
                    jmp lp82  
                lp83: 
                    cmp cx,430
                    jl  lp84
                    
                    MOV  AH,0CH
                    INT  10H
                    dec cx
                    jmp lp83  
                lp84: 
                    cmp dx,130
                    jl  ex8
                    
                    MOV  AH,0CH
                    INT  10H
                    dec dx
                    jmp lp84  
                    
                  ex8:
                   pop ax
                  pop dx
                  pop cx 
                  JMP NEXT_
           h9:
            mov al,1111b
              mov dx,160
              mov cx,420 
              mukh:
              cmp cx,430
              jg lose
              MOV  AH,0CH
              INT  10H 
              inc cx 
              jmp mukh
           
        win:
        push ax
         MOV AH,02
        MOV BH,0 
        MOV DH,18 ; ROW
        MOV DL,8 ; COL
        int 10h
        mov bl,1111b  
        mov si,0
        MOV BH,0 ; PAGE
       
        wn:
        cmp winner[si],'$'
        je exit 
        mov ah,09
    	mov bh,0
    	mov al,winner[si]
    	
    	mov cx,1
    	int 10h
    	inc si
    	inc dl
    	mov ah,02
    	int 10h
    	
    	jmp wn
    	
    	lose:
    	 push ax
         MOV AH,02
        MOV BH,0 
        MOV DH,18 ; ROW
        MOV DL,8 ; COL
        int 10h
        mov bl,1111b  
        mov si,0
        MOV BH,0 ; PAGE
       
        ls:
        cmp loser[si],'$'
        je exit 
        mov ah,09
    	mov bh,0
    	mov al,loser[si]
    	
    	mov cx,1
    	int 10h
    	inc si
    	inc dl
    	mov ah,02
    	int 10h
    	
    	jmp ls
        
        exit: 
        pop ax
        pop dx
        pop cx
        mov ax,10000
         mov si,0
         MOV LEN[si],0
         mov c1[si],0
         mov c2[si],0
        sd2:
           mov INP[SI],32d
           mov HANG[SI],36d
           
           INC SI
           cmp si,20
           jne sd2
           
          sd1:
          mov bx,200
          sf1:
          dec bx
          cmp bx,0
          jne sf1
           dec ax
          cmp ax,0
          jne sd1
          pop bx
          pop ax 
        pop si
        pop di
        
		ret
             hangman endp
        end 
        
