extrn clr:near,hangman:near
.model small
.stack 100h
.data 
mnu db 'MENU$'
CM db 'COLOR MANIA$'
HM db 'HANGMAN$'
e db 'EXIT$'

.code  


grid proc
     
    push si
    push ax
    push cx
    push dx   
    push bx
    
     
    mov cx,200
    mov dx,150
    mov al,4 
    
    ;;ROW CREATING LOOP
    
    lp1:
        MOV  AH,0CH
        INT  10H
        cmp cx,401
        je lp2
        inc cx
        jmp lp1
    
    lp2:
        add dx,50
        cmp dx,350
        JgE NEXT
        mov cx,200
        jmp lp1
    
    ;;COLUMN CREATING LOOP
    NEXT:
        MOV CX,200
        MOV DX,150
    lp3:
        MOV  AH,0CH
        INT  10H
        cmp DX,301
        je lp4
        inc DX
        jmp lp3
        
    
    lp4:
    mov cx,400
    mov dx,150
    pl:
        
        cmp DX,301
        JE  retr
        MOV  AH,0CH
        INT  10H
        inc dx
        JMP pl

retr: 
    pop bx
    pop dx
    pop cx
    pop ax 
    pop si
    ret
    grid endp


main proc
    mov ax,@data
    mov ds,ax
  
  mn:  
    
   mov AH, 0   ; graphics mode
    mov AX, 12h
    int 10h 
    
    
    MOV  AH, 11  ;function  OBh 
    MOV  BH, 0  ;select  background  color 
    MOV  BL,11 ;background color 7,11,15 possibly
    INT	10H
    
    call grid
    
     
     
    MOV AH,02
    MOV BH,0 
    MOV DH,8 ; ROW
    MOV DL,35 ; COL
    int 10h
    mov bl,0100b  
    mov si,0
    MOV BH,0 ; PAGE
     
    M1:
    cmp mnu[si],'$'
    je km
    mov bl,0100b 
    mov ah,09
	mov bh,0
	mov al,mnu[si]
	
	mov cx,1
	int 10h
	inc si
	inc dl
	mov ah,02
	int 10h
	
	jmp M1 
     
    km:
    MOV AH,02
    MOV BH,0 
    MOV DH,10 ; ROW
    MOV DL,33 ; COL
    int 10h
    mov bl,0100b  
    mov si,0
    MOV BH,0 ; PAGE
   
    p1:
    cmp CM[si],'$'
    je P
    mov bl,0100b 
    mov ah,09
	mov bh,0
	mov al,CM[si]
	
	mov cx,1
	int 10h
	inc si
	inc dl
	mov ah,02
	int 10h
	
	jmp p1
	
	P:
	MOV AH,02
    MOV BH,0 
    MOV DH,13
     ; ROW
    MOV DL,33 ; COL
    int 10h
    mov bl,0100b  
    mov si,0
    MOV BH,0 ; PAGE
     
     p2:
    cmp HM[si],'$'
    je exi 
    mov bl,0100b 
    mov ah,09
	mov bh,0
	mov al,HM[si]
	
	mov cx,1
	int 10h
	inc si
	inc dl
	mov ah,02
	int 10h
	
	jmp p2
     
     
     
     exi:
        MOV AH,02
        MOV BH,0 
        MOV DH,16
         ; ROW
        MOV DL,33 ; COL
        int 10h
        mov bl,0100b  
        mov si,0
        MOV BH,0 ; PAGE
         
         p3:
        cmp e[si],'$'
        je inp 
        mov bl,0100b 
        mov ah,09
    	mov bh,0
    	mov al,e[si]
    	
    	mov cx,1
    	int 10h
    	inc si
    	inc dl
    	mov ah,02
    	int 10h
    	
    	jmp p3
         
    
    inp:
     mov ax,0001h
    int 33h
    mov ax,0003h
    int 33h
    cmp bx,1
    jne inp
   
   
    cmp dx,150
    jl inp
    
    cmp dx,300
    jg inp
    
    CMP cx,200
    
    jl inp
    
    cmp cx,350
    jg inp
    
    
    
    cmp dx,200
    jl clrcall
     
     
     
    cmp dx,250
    jl hangcall
        
        
    cmp dx,250
    jl inp
    cmp dx,300
    jl exit
        
     
     clrcall:
    call clr
     jmp mn
     
      hangcall:
        call hangman
        jmp mn 
        
        
    exit:
    mov ah,4ch
    int 21h
    main endp
end main
    
    
    